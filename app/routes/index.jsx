import {
  useLoaderData,
  useActionData,
  Form,
  Link,
  useTransition,
} from "@remix-run/react";
import {
  unstable_createFileUploadHandler,
  unstable_parseMultipartFormData,
  json,
  redirect,
} from "@remix-run/node";
import prisma from "~/utils/db.server";
import Overlay from "~/components/Overlay";
import { useState } from "react";
import { ThreadReply } from "~/components/ThreadReply";

export async function action({ request }) {
  const clonedData = request.clone();
  const formData = await clonedData.formData();
  const title = formData.get("title");
  const post = formData.get("post");

  const fileUploadHandler = unstable_createFileUploadHandler({
    directory: "./public/uploads/",
    maxPartSize: 500000,
    file: ({ filename }) => {
      if (formData.get("anonymize"))
        return `${require("crypto").randomBytes(16).toString("hex")}.${
          filename.split(".").slice(-1)[0]
        }`;

      return filename;
    },
    filter: (data) => {
      const fileTypes = ["jpeg", "jpg", "png", "gif"];
      // if sent file is not an image, don't handle it
      if (!fileTypes.includes(data.contentType.split("/")[1])) return false;

      return true;
    },
  });

  const errors = {};
  let multiPartformdata;
  let imageName;
  try {
    multiPartformdata = await unstable_parseMultipartFormData(
      request,
      fileUploadHandler
    );
    imageName = multiPartformdata.get("image").name;
  } catch (err) {
    errors.image = "Image size too big or file sent is not an image";
  }

  if (typeof title !== "string" || title.length > 50 || title.length < 3) {
    errors.title = "Title too long or short";
  }
  if (typeof post !== "string" || post.length > 50 || post.length < 3) {
    errors.post = "Post too long or short";
  }
  if (Object.keys(errors).length) {
    return json(errors, { status: 422 });
  }

  const newThread = await prisma.thread.create({
    data: {
      title: title,
      post: post,
      imageName: imageName,
    },
  });

  return redirect(`threads/${newThread.id}`);
}

export async function loader({ request }) {
  const url = new URL(request.url);
  const term = url.searchParams.get("search-term");

  if (term) {
    if (typeof term !== "string") throw new Error("bad search term");
    const data = await prisma.thread.findMany({
      where: {
        OR: [
          {
            title: {
              contains: term,
            },
          },
          {
            post: {
              contains: term,
            },
          },
        ],
      },
      include: {
        posts: true,
      },
    });

    return data;
  }

  const data = await prisma.thread.findMany({
    include: {
      posts: {
        include: {
          replies: true,
        },
      },
    },
  });

  return data;
}

export default function Index() {
  const data = useLoaderData();
  const errors = useActionData();
  const transition = useTransition();
  const [search, setSearch] = useState("");

  return (
    <Overlay>
      <div className="mx-auto w-fit space-y-4 rounded-xl bg-ctp-crust p-10 shadow-lg shadow-ctp-red outline">
        <h1 className="bg-gradient-to-br from-ctp-subtext0 to-ctp-subtext1 bg-clip-text text-center text-4xl font-semibold text-transparent ">
          Create a thread
        </h1>
        <Form
          className="flex flex-wrap items-center justify-center space-y-4 rounded text-center "
          method="post"
          encType="multipart/form-data"
        >
          <div className="flex flex-wrap items-center justify-center">
            <label className="m-2 flex flex-col" htmlFor="title-input">
              <span className="font-semibold text-ctp-subtext0">Title</span>
              <textarea
                className=" m-1 rounded-md bg-ctp-surface0 p-1 shadow shadow-ctp-overlay0"
                required
                minLength={3}
                type="text"
                name="title"
                placeholder="Lain"
              />
              <p className="text-ctp-red">{errors ? errors.title : ""}</p>
            </label>
            <label className="m-2 flex flex-col" htmlFor="post-input">
              <span className="font-semibold text-ctp-subtext0">Post</span>
              <textarea
                className=" m-1 rounded bg-ctp-surface0 p-1 shadow shadow-ctp-overlay0"
                required
                minLength={3}
                type="text"
                name="post"
                placeholder="Iwakura"
              />
              <p className="text-ctp-red">{errors ? errors.post : ""}</p>
            </label>
            <label className="m-2 flex flex-col" htmlFor="image-upload">
              <span className="font-semibold text-ctp-subtext0">
                Image (500kbps max)
              </span>
              <input
                className="m-0 block w-full rounded bg-ctp-surface0 bg-clip-padding px-3 py-1.5 text-base font-semibold text-ctp-text transition ease-in-out"
                required
                accept="image/*"
                type="file"
                id="image-upload"
                name="image"
              />
              <label htmlFor="anon">
                <span>Anonymize filename </span>
                <input type="checkbox" name="anonymize" id="anon-check" />
              </label>
              <p className="text-ctp-red">{errors ? errors.image : ""}</p>
            </label>
          </div>
          <button
            className=" hover rounded-full bg-ctp-surface1 py-2 px-4 text-ctp-flamingo shadow shadow-ctp-overlay0 hover:bg-ctp-surface2 hover:underline"
            type="submit"
          >
            {transition.state === "submitting" ? "Creating..." : "Create!"}
          </button>
        </Form>
      </div>
      <Form onSubmit={(e) => e.preventDefault()} className="pt-8" method="get">
        <label className="flex flex-col items-center" htmlFor="thread-search">
          <span className="text-2xl tracking-widest">
            {search ? `Searching with term: ${search}` : "Search for a thread"}
          </span>
          <input
            onChange={(e) => setSearch(e.target.value)}
            value={search}
            className="m-1 w-fit rounded bg-ctp-surface0 p-1 shadow shadow-ctp-overlay0"
            placeholder="Search term..."
            type="text"
            name="search-term"
            id="search"
          />
        </label>
      </Form>
      {data.length > 0 ? (
        <ul className="flex flex-wrap items-center justify-around p-8">
          {data.map((thread) =>
            thread.title.includes(search) || thread.post.includes(search) ? (
              <li
                className={`group m-4 scale-100 rounded-xl bg-ctp-mantle p-4 shadow-xl shadow-ctp-pink outline duration-300 hover:scale-110 hover:shadow-xl`}
                key={thread.id}
              >
                <Link
                  className="flex items-center justify-center gap-4 space-y-4"
                  to={`threads/${thread.id}`}
                >
                  <div>
                    <div className="flex items-center justify-center">
                      <img
                        className="max-h-96 w-48 rounded-xl "
                        src={`/uploads/${thread.imageName}`}
                        alt="thread image"
                      />
                    </div>
                    <div>
                      <h2 className="text-center text-2xl font-semibold tracking-tighter ">
                        {thread.title}
                      </h2>
                      <p className=" text-center text-ctp-subtext0">
                        {thread.post}
                      </p>
                      <p className=" text-center text-xs text-ctp-subtext1 ">
                        Posts: {thread.posts.length}
                      </p>
                    </div>
                  </div>
                  <div className="hidden flex-col items-center justify-center group-hover:flex">
                    <h1 className="text-center text-2xl tracking-tighter">
                      Last {thread.posts.slice(-3).length} post(s)
                    </h1>
                    <ul className="">
                      {thread.posts.length > 0 ? (
                        thread.posts
                          .slice(-3)
                          .map((post) => (
                            <ThreadReply
                              index={true}
                              key={post.id}
                              post={post}
                            />
                          ))
                      ) : (
                        <li>post has no replies yet...</li>
                      )}
                    </ul>
                  </div>
                </Link>
              </li>
            ) : (
              ""
            )
          )}
        </ul>
      ) : (
        <h3 className="flex items-center justify-center p-20 text-center text-2xl">
          no threads yet
        </h3>
      )}
    </Overlay>
  );
}

export function ErrorBoundary({ error }) {
  return (
    <Overlay>
      <div className="w-full rounded bg-ctp-red p-4 text-white">
        <h1 className="text-2xl">{error.message}</h1>
        <code>{error.stack}</code>
      </div>
    </Overlay>
  );
}
