import { Link } from "@remix-run/react";

export default function Header() {
  return (
    <div className="mx-auto mb-12 flex items-center justify-center space-x-4 rounded bg-ctp-crust py-6 px-12 shadow-lg shadow-ctp-sapphire outline">
      <div className="flex-1"></div>
      <div className="">
        <h1 className="text-4xl font-bold">Lauta</h1>
      </div>
      <nav className="flex flex-1 justify-end">
        <Link
          className="text-ctp-blue hover:text-ctp-teal hover:underline"
          to={"/"}
        >
          threads
        </Link>
      </nav>
    </div>
  );
}
