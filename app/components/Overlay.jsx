import Header from "./Header";

export default function Overlay({ children }) {
  return (
    <div className={`container mx-auto p-4 text-ctp-text`}>
      <Header />
      {children}
    </div>
  );
}
