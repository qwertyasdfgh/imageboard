import { Link } from "@remix-run/react";

export function ThreadReply({ post, setReplying, index }) {
  return (
    <li
      id={`${post.id}`}
      className="m-4 w-fit rounded border p-4 shadow odd:bg-ctp-mantle even:bg-ctp-crust"
      key={post.id}
    >
      <div className="flex flex-wrap">
        <span>
          Reply id: <strong>{post.id}</strong> Replied at:{" "}
          <strong>{new Date(post.createdAt).toLocaleString()} </strong>
        </span>
        <Link
          className="mx-2 text-ctp-rosewater hover:text-ctp-maroon hover:underline"
          onClick={(event) => {
            index ? "" : event.preventDefault();
            document.getElementById(`bottom`).scrollIntoView(true);
            setReplying(post.id);
          }}
          to={`/threads/${post.postId}#bottom`}
        >
          Reply
        </Link>
        <ul className="flex flex-wrap space-x-1">
          {post?.replies?.map((reply) => (
            <li key={reply.id}>
              <Link
                onClick={(event) => {
                  index ? "" : event.preventDefault();
                  document.getElementById(`${reply.id}`).scrollIntoView(true);
                }}
                className="text-ctp-teal hover:text-ctp-sky hover:underline"
                to={`#${reply.id}`}
              >
                #{reply.id}
              </Link>
            </li>
          ))}
        </ul>
      </div>
      {post.replyingTo ? (
        <Link
          onClick={(event) => {
            index ? "" : event.preventDefault();
            document.getElementById(`${post.replyingTo}`).scrollIntoView(true);
          }}
          className="text-sm font-semibold hover:underline"
          to={`/threads/${post.postId}#${post.replyingTo}`}
        >
          Replying to id: {post.replyingTo}
        </Link>
      ) : (
        ""
      )}
      {post.imageName !== null ? (
        <div className="flex flex-col">
          <img
            className="max-h-96 w-60"
            src={`/uploads/${post.imageName}`}
            alt="post image"
          />
          <Link
            className=" text-xs text-ctp-surface0"
            to={`/uploads/${post.imageName}`}
            target="_blank"
            referrerPolicy="no-referrer"
          >
            Click to show full image
          </Link>
        </div>
      ) : (
        ""
      )}
      <p>{post.comment}</p>
    </li>
  );
}
