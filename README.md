# Imageboard

An imageboard made with Remix, with an SQLite database, using Prisma ORM. Also using Tailwind for styling.
Not ready for production yet, but you can try.

The board works almost completely without JavaScript (thread making, replying to threads), but user experience is very much improved if you enable it.

## Development

Format the code with Prettier using the command line (check the documentation), or just install the Prettier extension from VSCode extensions.

```sh
git clone https://git.disroot.org/qwertyasdfgh/imageboard
cd imageboard
pnpm install
pnpm prisma db push
pnpm run dev
```

Set DATABASE_URL with .env

## Production

```sh
git clone https://git.disroot.org/qwertyasdfgh/imageboard
cd imageboard
pnpm install
pnpm prisma generate
pnpm prisma migrate deploy
pnpm run build
pnpm start
```

Run behind reverse proxy like NGINX
